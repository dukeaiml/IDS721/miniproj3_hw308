import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as iam from 'aws-cdk-lib/aws-iam';

export class Miniproj3Hw308Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create an S3 Bucket enabling versioning and encryption
    const myBucket = new cdk.aws_s3.Bucket(this, 'miniproj3_hw308', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED,
      removalPolicy: cdk.RemovalPolicy.DESTROY
    });
    //allow the root user of the AWS account to read contents in this S3 bucket
    myBucket.grantRead(new iam.AccountRootPrincipal());
  }
}
