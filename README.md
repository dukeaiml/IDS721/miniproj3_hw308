# miniproj3_hw308
This project created an S3 Bucket using CDK with AWS CodeWhisperer.

## Getting started

1. Create a new environment from AWS CodeCawtalyst
2. Open a new Cloud Development Environment in AWS Cloud 9.
3. Create a new user in AWS IAM and set up its corresponding credentials.
4. aws configure in Cloud9
5. Follow the first half of the instructions [Your first AWS CDK app](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html) to create the project
6. Use CodeWhisperer to generate the code for creating a S3 bucket (attached graphs in the next section)
7. Follow the rest instructions in the link above. (cdk synth, cdk deploy)
8. You can see the deployed bucket on AWS.

## Graph Verifications 

- CodeWhisperer opening & S3 Bucket Creation on Cloud9

![](images/cw_open.png)


- S3 Bucket Versioning and Encryption Enabled

![](images/bucket_versioning.png)

![](images/encryption.png)